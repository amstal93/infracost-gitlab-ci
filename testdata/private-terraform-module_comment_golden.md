
💰 Infracost estimate: **monthly cost will increase by $25.40 (+223%) 📈**
<table>
  <thead>
    <td>Project</td>
    <td>Previous</td>
    <td>New</td>
    <td>Diff</td>
  </thead>
  <tbody>
    <tr>
      <td>infracost/infracost-gitlab-ci/e.../private-terraform-module/code</td>
      <td align="right">$11.37</td>
      <td align="right">$36.77</td>
      <td>+$25.40 (+223%)</td>
    </tr>
  </tbody>
</table>

<details>
<summary><strong>Infracost output</strong></summary>

```
Project: infracost/infracost-gitlab-ci/examples/private-terraform-module/code

~ module.ec2_cluster.aws_instance.this[0]
  +$25.40 ($11.37 → $36.77)

    ~ Instance usage (Linux/UNIX, on-demand, t2.micro → t2.medium)
      +$25.40 ($8.47 → $33.87)

Monthly cost change for infracost/infracost-gitlab-ci/examples/private-terraform-module/code
Amount:  +$25.40 ($11.37 → $36.77)
Percent: +223%

──────────────────────────────────
Key: ~ changed, + added, - removed

1 cloud resource was detected:
∙ 1 was estimated, it includes usage-based costs, see https://infracost.io/usage-file
```
</details>

This comment will be updated when the cost estimate changes.

<sub>
  Is this comment useful? <a href="https://www.infracost.io/feedback/submit/?value=yes" rel="noopener noreferrer" target="_blank">Yes</a>, <a href="https://www.infracost.io/feedback/submit/?value=no" rel="noopener noreferrer" target="_blank">No</a>
</sub>

Comment not posted to GitLab (--dry-run was specified)
